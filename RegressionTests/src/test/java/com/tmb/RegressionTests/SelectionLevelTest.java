package com.tmb.RegressionTests;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.com.tmb.Locators.SelectionLevelLocators;
import org.com.tmb.config.CommonConstants;
import org.com.tmb.hooks.Hooks;
import org.com.tmb.hooks.WaitFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

public class SelectionLevelTest extends Hooks {

	@Parameters({ "browser" })
	@BeforeMethod
	public void openBrowser(String browserName) {
		Hooks.OpenBrowser(browserName);
		Hooks.getOSName();

	}

	/*
	 * Test Case: 1 1) From dashBoards select Registrations
	 *  2) Network as Administrative
	 *  expected : Selection level should have options as Country Regions and UK Sector
	 * 
	 */
	@Test(priority = 1)
	public void testDashSelectionLevels() throws InterruptedException,Exception {
		SoftAssert sa = new SoftAssert();
		String[] actual_result = new String[2];
		actual_result[0] = "Country Regions";
		actual_result[1] = "UK Sectors";
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(SelectionLevelLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(SelectionLevelLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(SelectionLevelLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		// select the Dashboard type as Deliveries
		Select Deliveries_Dropdown = new Select(driver.findElement(SelectionLevelLocators.DashBoard_Type));
		Deliveries_Dropdown.selectByVisibleText(SelectionLevelLocators.Dashboard_Reg_dropdown_value);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Actions action = new Actions(driver);
		WebElement scrollbar = driver.findElement(By.xpath(".//*[@id='mCSB_1_dragger_vertical']"));
		action.moveToElement(scrollbar).moveByOffset(0, 150).click().perform();
		WaitFunctions.wait6();
		Select dropdown = new Select(driver.findElement(SelectionLevelLocators.SelectionLevel_type));
		List<WebElement> all_options = dropdown.getOptions();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		System.out.println(" -->" + all_options.size());
		for (int j = 0; j < all_options.size(); j++) {
			System.out.println(j);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			String[] expected_values = new String[2];
			expected_values[j] = all_options.get(j).getText();
			System.out.println("Expected Drop down values are  :  " + expected_values[j]);
			if(expected_values[j].equals (actual_result[j]))
			{
				System.out.println("Test Passed, Network dropdown options(Country  Regions and UK Sector) are  Equal");
				Hooks.captureScreenshot("testDashSelectionLevels","Test Passed");
				et.log(LogStatus.PASS,"Test Passed, Network dropdown options(Country  Regions and UK Sector) are  Equal");

			} else
			{
				System.out.println("Test Passed, Network dropdown options(Country  Regions and UK Sector) are not Equal");
				Hooks.captureScreenshot("testDashSelectionLevels","Test failed");
				et.log(LogStatus.FAIL,"Test Passed, Network dropdown options(Country  Regions and UK Sector) are not Equal");
			}
			
			
			sa.assertEquals(expected_values[j], actual_result[j]);
		}

	}

	/*
	 * Test Case : 2 1) Change back it to Deliveries 2) Network as
	 * Administrative expected : Selection level should have option UK Sectors
	 */
	@Test(priority = 2)
	public void testDeliveriesselectionlevels() throws Exception {
		SoftAssert sa = new SoftAssert();
		String actual_result = "UK Sectors";
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(SelectionLevelLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(SelectionLevelLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(SelectionLevelLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		// select the Dashboard type as Deliveries
		Select Deliveries_Dropdown = new Select(driver.findElement(SelectionLevelLocators.DashBoard_Type));
		Deliveries_Dropdown.selectByVisibleText(SelectionLevelLocators.Dashboard_Del_dropdown_value);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Actions action = new Actions(driver);
		WebElement scrollbar = driver.findElement(By.xpath(".//*[@id='mCSB_1_dragger_vertical']"));
		action.moveToElement(scrollbar).moveByOffset(0, 150).click().perform();
		WaitFunctions.wait6();
		// Select the Network Type as Administrative
		Select Network_Dropdown = new Select(driver.findElement(By.id("field-network_id")));
		Network_Dropdown.selectByVisibleText(SelectionLevelLocators.Network_Administrative);
		Select dropdown = new Select(driver.findElement(SelectionLevelLocators.SelectionLevel_type));
		List<WebElement> all_options = dropdown.getOptions();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		System.out.println(" -->" + all_options.size());
		for (int j = 0; j < all_options.size(); j++) {
			System.out.println(j);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			String expected_result = all_options.get(j).getText();
			System.out.println("Expected Drop down values in Test2 are  :  " + expected_result);
			if(expected_result.equals(actual_result))
			{
				System.out.println("Test Passed," + expected_result + "and" + actual_result+ " are  Equal");
				Hooks.captureScreenshot("testDeliveriesselectionlevels","Test Passed");
				et.log(LogStatus.PASS,"Test Passed, Expected Deliveries_count and map_value are Equal");

			} else
			{
				System.out.println("Test Passed," + expected_result + "and" + actual_result+ " are not Equal");
				Hooks.captureScreenshot("testDeliveriesselectionlevels","Test failed");
				et.log(LogStatus.FAIL,"Test Failed, Expected Deliveries_count and map_value are not Equal");
			}
			sa.assertEquals(actual_result, expected_result);
			sa.assertAll();

		}
	}

	/*
	 * Test Cse : 3 1) Test case -3 2) Change back it to Deliveries 3) Network
	 * as Administrative
	 * 
	 * Expected : selection level should be UK Sectors
	 */

	@AfterMethod
	public void closeDriver() {
		driver.quit();
		er.flush();
		er.endTest(et);

	}
}
