package com.tmb.RegressionTests;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.com.tmb.Locators.AllDeliveriesLocators;
import org.com.tmb.config.CommonConstants;
import org.com.tmb.hooks.Hooks;
import org.com.tmb.hooks.WaitFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;


public class AllDeliveriesTest extends Hooks {

	@Parameters({ "browser" })
	@BeforeMethod
	public void openBrowser(String browserName){
		Hooks.OpenBrowser(browserName);
		Hooks.getOSName();
		
	}

	
@Test(priority = 1)
public void testRegistrationsReportType() throws Exception,MalformedURLException,InterruptedException {

	SoftAssert sa= new SoftAssert();
	driver.get(CommonConstants.TMB_STG_URL);
	driver.manage().window().maximize();
	driver.findElement(AllDeliveriesLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
	driver.findElement(AllDeliveriesLocators.TMB_Password).sendKeys(CommonConstants.password);
	driver.findElement(AllDeliveriesLocators.TMB_Submit_Button).click();
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS); 
	  //select the Dashboard type as Deliveries
		Select Deliveries_Dropdown = new Select(driver.findElement(By.id("field-dashboard_id")));
		Deliveries_Dropdown.selectByVisibleText("Deliveries");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//select the Brand type as Citreon
		driver.findElement(By.xpath("//*[@id='panel4']/form/div/div[2]/div[1]/div/a")).click();
		driver.findElement(By.xpath("//input[@id='field-brand-selectized']")).clear();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		WebElement brand = driver.findElement(By.xpath("//input[@id='field-brand-selectized']"));
		brand.sendKeys("cit");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		AllDeliveriesLocators.selectCar(driver, "Citroen");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//select the Section type as All sections
		Select All_Sections = new Select(driver.findElement(By.id("field-chart_id")));
		All_Sections.selectByVisibleText("Pump In Pump Out Analysis");
		System.out.println("Now moving the first vertical scroll bar");
		WaitFunctions.wait6();
		//Move the vertical scroll bar bit down.
		Actions action =new Actions(driver);
		WebElement scrollbar=driver.findElement(By.xpath(".//*[@id='mCSB_1_dragger_vertical']"));
		action.moveToElement(scrollbar).moveByOffset(0,150).click().perform();
		WaitFunctions.wait6();
		System.out.println("Moved the first vertical scroll bar");
		//Select the Network Type as NV.
		Select Network_Dropdown = new Select(driver.findElement(By.id("field-network_id")));
		Network_Dropdown.selectByVisibleText("NV");
		//Select the Network Type as UK Citroen AOIs
		Select Selection_Level_Dropdown = new Select(driver.findElement(By.id("field-territoryset_id")));
		Selection_Level_Dropdown.selectByVisibleText("UK Citroen AOIs");
		WebElement Terrioty=driver.findElement(By.xpath("(.//input[@id='field-territory_id-selectized'])[1]"));
		Terrioty.click();
		action.sendKeys("Arnold Clark (Edinburgh - 50058) [001681T]").build().perform();
		WaitFunctions.wait2();
		driver.findElement(By.xpath(".//*[@id='panel2']/form/div[5]/div[2]/div[2]/div/div[1]")).click();
		driver.findElement(By.xpath("(//div[@class='result-information'])[1]/span[1]")).click();
		System.out.println("Selected Arnold Clark (Edinburgh - 50058) [001681T]");
		WaitFunctions.wait10();
		String Deliveries_count=driver.findElement(By.xpath("(//div[@class='result-information'])[1]/span[1]")).getText();
		System.out.println("Deliveries_count=====>"+Deliveries_count);
		WaitFunctions.wait10();
		//mCustomScrollBox mCS-dark-thick mCSB_vertical mCSB_inside
		System.out.println("Now moving the second vertical scroll bar");
		WebElement verticalbar=driver.findElement(By.xpath(".//*[@id='mCSB_2_dragger_vertical']/div"));
		WaitFunctions.wait10();
		action.moveToElement(verticalbar).moveByOffset(0,150).click().perform();
		System.out.println("Moved the second vertical scroll bar");
        //JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement map_value=driver.findElement(By.xpath("//td[.='Total Dealer Deliveries']//following-sibling::td[1]"));
       // js.executeScript("arguments[0].scrollIntoView();", v1);
        String expectedValue =map_value.getText();
		System.out.println("2");
		System.out.println("map_value=====>"+map_value);
		if(Deliveries_count.equals(expectedValue))
		{
			System.out.println("Test Passed, Expected Deliveries_count and map_value are Equal");
			Hooks.captureScreenshot("verifyRegistrationsReportType","Test Passed");
			et.log(LogStatus.PASS,"Test Passed, Expected Deliveries_count and map_value are Equal");

		} else
		{
			System.out.println("Test failed, Expected Deliveries_count and map_value are not Equal");
			Hooks.captureScreenshot("verifyRegistrationsReportType","Test failed");
			et.log(LogStatus.FAIL,"Test Failed, Expected Deliveries_count and map_value are not Equal");
		}
		sa.assertEquals(Deliveries_count, map_value);	
		
		
	}





@AfterMethod
public void closeDriver() {
	driver.quit();
	er.flush();
	er.endTest(et);
}

}
