package com.tmb.RegressionTests;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.com.tmb.Locators.AllDeliveriesLocators;
import org.com.tmb.Locators.TIVLocators;
import org.com.tmb.config.CommonConstants;
import org.com.tmb.hooks.Hooks;
import org.com.tmb.hooks.WaitFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

public class TIVTest extends Hooks {
	
	SoftAssert sa= new SoftAssert();

	
	@Parameters({ "browser" })
	@BeforeMethod
	public void openBrowser(String browserName){
		Hooks.OpenBrowser(browserName);
		Hooks.getOSName();

	}
	
	/*Dashbaords->Registrationss
	 * Read the value from Registrations Header
	 * From deliveries select the value as Dealer Penetration in Territory
	 * Navigate to the  Dealer Penetration in Territory TIV value and compare the value against Registrations value
	 * */
	@Test(priority = 1)
	public void verifyDealerPenetrationTIV() throws MalformedURLException,InterruptedException,Exception
	{
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(TIVLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(TIVLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(TIVLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS); 
		System.out.println("in the verifydeleaerPenetrationTIV ");
		String Reg_HeaderValue= driver.findElement(By.xpath("//div[@class='result-information']/span[1]")).getText();
		System.out.println("Reg_HeaderValue====>"+Reg_HeaderValue);
		//select the Dashboard type as Deliveries
		Select Deliveries_Dropdown = new Select(driver.findElement(By.id("field-dashboard_id")));
		Deliveries_Dropdown.selectByVisibleText("Deliveries");
		//select the Section type as Dealer Penetration in Territory
		Select All_Sections = new Select(driver.findElement(By.id("field-chart_id")));
		All_Sections.selectByVisibleText("Dealer Penetration in Territory");
		WaitFunctions.wait10();
		String Dealer_Pen_Territory=driver.findElement(By.xpath((".//*[@id='row1']/td[4]/a"))).getText();
		Thread.sleep(6000);
		System.out.println("Dealer_Pen_Territory :"+Dealer_Pen_Territory);
		if(Reg_HeaderValue.equals(Dealer_Pen_Territory))
		{
			System.out.println("Test Passed, Expected Reg_HeaderValue and Dealer_Pen_Territory are Equal");
			Hooks.captureScreenshot("verifyDealerPenetrationTIV","Test Passed");
			et.log(LogStatus.PASS,"Test Passed, Expected Reg_HeaderValue and Dealer_Pen_Territory are Equal");

		} else
		{
			System.out.println("Test Passed, Expected Reg_HeaderValue and Dealer_Pen_Territory are Equal");
			Hooks.captureScreenshot("verifyDealerPenetrationTIV","Test failed");
			et.log(LogStatus.FAIL,"Test Failed, Expected Reg_HeaderValue and Dealer_Pen_Territory are not Equal");
		}
	    sa.assertEquals(Reg_HeaderValue, Dealer_Pen_Territory);
        sa.assertAll();
	}
	
	/*Dashbaords->Registrations
	 * Read the value from Registrations Header
	 * From deliveries select the value as Dealer Penetration in Territory
	 * Navigate to the  In Territory Postcode Focus TIV value and compare the value against Registrations value*/
	 
	@Test(priority = 2)
	public void verifyTerritoryPostcodeFocus() throws MalformedURLException,InterruptedException,Exception
	{
		driver.manage().deleteAllCookies();
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(TIVLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(TIVLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(TIVLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS); 
		System.out.println("in the verifyTerritoryPostcodeFocus ");
		Select Deliveries_Dropdown = new Select(driver.findElement(By.id("field-dashboard_id")));
		Deliveries_Dropdown.selectByVisibleText("Registrations");
		Thread.sleep(5000);
		System.out.println("in the verifydeleaerPenetrationTIV ");
		String Reg_HeaderValue= driver.findElement(By.xpath("//div[@class='result-information']/span[1]")).getText();
		System.out.println("Reg_HeaderValue verifyTerritoryPostcodeFocus====>"+Reg_HeaderValue);
		//select the Dashboard type as Deliveries
		Select Deliveries_Dropdowns = new Select(driver.findElement(By.id("field-dashboard_id")));
		Deliveries_Dropdowns.selectByVisibleText("Deliveries");
		//select the Section type as Dealer Penetration in Territory
		Select All_Sections = new Select(driver.findElement(By.id("field-chart_id")));
		All_Sections.selectByVisibleText("In Territory Postcode Focus");
		String Territory_postcode_Focus=driver.findElement(By.xpath((".//*[@id='row1']/td[4]"))).getText();
		WaitFunctions.wait6();	
		System.out.println("Territory_postcode_Focus :"+Territory_postcode_Focus);
		if(Reg_HeaderValue.equals(Territory_postcode_Focus))
		{
			System.out.println("Test Passed, Expected Reg_HeaderValue and Territory_postcode_Focus are Equal");
			Hooks.captureScreenshot("verifyTerritoryPostcodeFocus","Test Passed");
			et.log(LogStatus.PASS,"Test Passed, Expected Reg_HeaderValue and Territory_postcode_Focus are Equal");

		} else
		{
			System.out.println("Test Passed, Expected Reg_HeaderValue and Territory_postcode_Focus are Equal");
			Hooks.captureScreenshot("verifyTerritoryPostcodeFocus","Test failed");
			et.log(LogStatus.FAIL,"Test Failed, Expected Reg_HeaderValue and Territory_postcode_Focus are not Equal");
		}
	    sa.assertEquals(Reg_HeaderValue, Territory_postcode_Focus);
	}
	
	@AfterMethod
	public void closeDriver() {
		driver.manage().deleteAllCookies();
		driver.quit();
		er.flush();
		er.endTest(et);
	}
}
