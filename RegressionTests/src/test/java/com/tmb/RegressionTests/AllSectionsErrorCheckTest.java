package com.tmb.RegressionTests;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.com.tmb.Locators.AllDeliveriesLocators;
import org.com.tmb.config.CommonConstants;
import org.com.tmb.hooks.Hooks;
import org.com.tmb.hooks.WaitFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

public class AllSectionsErrorCheckTest extends Hooks {
	

	@Parameters({ "browser" })
	@BeforeMethod
	public void openBrowser(String browserName){
		Hooks.OpenBrowser(browserName);
		Hooks.getOSName();
		
	}
	
	/*
	 * This test case selects the dashboard type as Registrations
	 * Navigates to Each Section and selects each section
	 * and verifies if any error present in it or not
	 * 
	 * */
	
	@Test(priority = 1)
	public void testRegistrationsReportType() throws Exception,MalformedURLException,InterruptedException {

		String errorText ="Error";
		SoftAssert sa= new SoftAssert();
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(AllDeliveriesLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(AllDeliveriesLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(AllDeliveriesLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS); 
		//select the Dashboard type as Deliveries
		Select Deliveries_Dropdown = new Select(driver.findElement(By.id("field-dashboard_id")));
		Deliveries_Dropdown.selectByVisibleText("Registrations");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//select the Section type as All sections
		Select All_Sections = new Select(driver.findElement(By.id("field-chart_id")));
		List <WebElement> elementCount = All_Sections.getOptions();
		int iSize = elementCount.size();
		for(int i =1; i<iSize ; i++){
			String sValue = elementCount.get(i).getText();
			System.out.println(sValue);
			if(driver.getPageSource().contains(errorText))
			{
				System.out.println("Error in the Registration-Selection Level report and it's name is "  +sValue);
				Hooks.captureScreenshot("testDeliveriesReportType","Test failed");
				et.log(LogStatus.PASS,"Test failed, All Report types for Registration dashbaords are not fine");

			}

			else
			{
				System.out.println("Registration-Selection Level reports are green for the following reports "  +sValue);
				Hooks.captureScreenshot("testDeliveriesReportType","Test Passed");
				et.log(LogStatus.PASS,"Test Passed, All Report types for Registration dashbaords are fine");

			}
			}
	
	}
	
	
	/*
	 * This test case selects the dashboard type as Registrations
	 * Navigates to Each Section and selects each section
	 * and verifies if any error present in it or not
	 * 
	 * */
	
	@Test(priority = 2)
	public void testDeliveriesReportType() throws Exception,MalformedURLException,InterruptedException {

		String errorText ="Error";
		SoftAssert sa= new SoftAssert();
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(AllDeliveriesLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(AllDeliveriesLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(AllDeliveriesLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS); 
		//select the Dashboard type as Deliveries
		Select Deliveries_Dropdown = new Select(driver.findElement(By.id("field-dashboard_id")));
		Deliveries_Dropdown.selectByVisibleText("Deliveries");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//select the Section type as All sections
		Select All_Sections = new Select(driver.findElement(By.id("field-chart_id")));
		List <WebElement> elementCount = All_Sections.getOptions();
		WaitFunctions.wait3();
		int iSize = elementCount.size();
		for(int i =1; i<iSize ; i++){
			boolean breakIt = true;
	        while (true) {
	        breakIt = true;
	        try {
				String sValue = elementCount.get(i).getText();
				System.out.println(sValue);
				if(driver.getPageSource().contains(errorText))
				{
					System.out.println("Error in the Deliveries-Selection Level report and it's name is "  +sValue);
					Hooks.captureScreenshot("testDeliveriesReportType","Test failed");
					et.log(LogStatus.PASS,"Test failed, All Report types for Registration dashbaords are not fine");

				}

				else
				{
					System.out.println("Deliveries-Selection Level reports are green for the following reports "  +sValue);
					Hooks.captureScreenshot("testDeliveriesReportType","Test Passed");
					et.log(LogStatus.PASS,"Test Passed, All Report types for Registration dashbaords are fine");

				}
	        } catch (Exception e) {
	            if (e.getMessage().contains("element is not attached")) {
	                breakIt = false;
	            }
	        }
	        if (breakIt) {
	            break;
	        }

	    }
			
			
			}
	
	}
	
	
	
	@AfterMethod
	public void closeDriver() {
		driver.manage().deleteAllCookies();
		driver.quit();
		er.flush();
		er.endTest(et);
	}

}
