package org.com.tmb.Locators;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;


public class AllDeliveriesLocators {
	
	
	//landing page elements
			public static By TMB_UserName = By.cssSelector("#UserEmail");
			public static By TMB_Password = By.cssSelector("#UserPassword");
			public static By TMB_Submit_Button =By.cssSelector("#loginformbutton");
			
			 //select the Dashboard type as Deliveries
			
			public static By DashBoard_Type = By.cssSelector("#field-dashboard_id");
	

			
	public static  void selectCar(WebDriver driver, String text){
		List<WebElement> elements = driver.findElements(By.className("option"));
		for (WebElement element : elements) {
			if (element.getText().equalsIgnoreCase("citroen")) {
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					
					element.click();
					
				driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
			}
		}
	}

}
