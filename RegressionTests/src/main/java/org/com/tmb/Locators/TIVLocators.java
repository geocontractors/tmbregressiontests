package org.com.tmb.Locators;

import org.openqa.selenium.By;

public class TIVLocators {
	
	//Login Credentials
	public static By TMB_UserName = By.cssSelector("#UserEmail");
	public static By TMB_Password = By.cssSelector("#UserPassword");
	public static By TMB_Submit_Button =By.cssSelector("#loginformbutton");

}
