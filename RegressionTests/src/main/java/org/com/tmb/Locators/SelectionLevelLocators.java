package org.com.tmb.Locators;

import org.openqa.selenium.By;

public class SelectionLevelLocators {
	
	
	//landing page elements.
	public static By TMB_UserName = By.cssSelector("#UserEmail");
	public static By TMB_Password = By.cssSelector("#UserPassword");
	public static By TMB_Submit_Button =By.cssSelector("#loginformbutton");
	
	 //select the Dashboards type as Deliveries
	public static By DashBoard_Type = By.cssSelector("#field-dashboard_id");
	//select selection levels id
	public static By SelectionLevel_type = By.cssSelector("#field-territoryset_id");

	
    public static String Dashboard_Reg_dropdown_value ="Registrations";
    public static String Dashboard_Del_dropdown_value ="Deliveries";
    
    //select the Network as Administrative
    
    public static String Network_Administrative ="Administrative";


}
